#!/usr/bin/env bash

# export FLASK_ENV='development'  # 'FLASK_ENV' is deprecated and will not be used in Flask 2.3. Use 'FLASK_DEBUG' instead.
export FLASK_DEBUG=1
export FLASK_APP='gnss-flask-control-emulator.py'
# :: — listen both IPv4 + IPv6 (in Linux, this depends on sysctl option `net.ipv6.bindv6only`)
flask run -p 8080 -h ::
