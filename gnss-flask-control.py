import os
import subprocess
import datetime
import psutil
from flask import Flask, render_template, request


def get_board_info():
    """
    Read raw data, normalize it (divide temperature to 1000, divide voltage/amperage to a million).
    Calculate power consumption for AC and battery.
    Based on `30-armbian-sysinfo` shell script.
    Should work with PMIC AXP209 and AXP803.
    """
    data_paths = {
        'AC connected': '/sys/power/axp_pmu/ac/connected',
        'Battery connected': '/sys/power/axp_pmu/battery/connected',
        'Battery charging': '/sys/power/axp_pmu/charger/charging',
        'Battery percent': '/sys/power/axp_pmu/battery/capacity',
        'AC voltage': '/sys/power/axp_pmu/ac/voltage',
        'AC amperage': '/sys/power/axp_pmu/ac/amperage',
        'Battery voltage': '/sys/power/axp_pmu/battery/voltage',
        'Battery amperage': '/sys/power/axp_pmu/battery/amperage',
        'CPU temperature': '/etc/armbianmonitor/datasources/soctemp',
        'PMIC temperature': '/etc/armbianmonitor/datasources/pmictemp'
    }
    data = {}
    # Read raw data
    for name, path in data_paths.items():
        try:
            with open(path) as f:
                data[name] = int(f.readline().rstrip())
                # Normalize data
                if (name.endswith('voltage')) or (name.endswith('amperage')):
                    data[name] = data[name] / 10**6
                elif name.endswith('temperature'):
                    data[name] = data[name] / 10**3
        except OSError:
            pass
    # Calculate power
    data['Power AC'] = data['AC voltage'] * data['AC amperage']
    data['Power battery'] = data['Battery voltage'] * data['Battery amperage']
    return data


def get_files_sort_max(path: str):
    max_mtime = 0
    files_list = []
    if os.path.exists(path):
        for entry in sorted(os.scandir(path), key=lambda d: d.stat().st_mtime, reverse=True):
            if not entry.name.startswith('.') and entry.is_file():
                if max_mtime < entry.stat().st_mtime:
                    max_mtime = entry.stat().st_mtime
                # Without strftime(), it return object like datetime.datetime(2021, 9, 4, 14, 41, 54, 30511)
                human_mtime = datetime.datetime.fromtimestamp(entry.stat().st_mtime).strftime('%Y-%m-%d %H:%M:%S')
                files_list.append('{} {} {}'.format(human_mtime, entry.stat().st_size, entry.name))
    if len(files_list) > 0:
        diff_max_mtime = datetime.datetime.today().timestamp() - max_mtime
    else:
        diff_max_mtime = 0
    return diff_max_mtime, files_list


def check_process(check_list: list):
    out = []
    processes_by_name = {p.name(): p.info for p in psutil.process_iter(['name', 'username'])}
    # TODO: add 'webgps.py' (which is actually a Python process, so it is not so easy to find it).
    for process_name in check_list:
        if process_name in processes_by_name:
            out.append('{} is <span style="background-color: green; color: grey;">running</span>'.format(process_name))
        else:
            out.append('{} is <span style="background-color: red; color: grey;">not running</span>'.format(process_name))
    return out


app = Flask(__name__)


@app.route("/")
def index_page():
    diff_max_mtime, files_list = get_files_sort_max('/tmp/gpspipe')
    time_now = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    return render_template('index.html.j2',
                           board_info=get_board_info(),
                           print_all_processes=check_process(['gpsd', 'gpspipe', 'hostapd']),
                           time_now=time_now,
                           diff_max_mtime=diff_max_mtime,
                           nmea_files=files_list)


@app.route("/favicon.ico")
def favicon_empty():
    return ''


@app.route('/button', methods=['GET', 'POST'])
def button():
    if "start" in request.form:
        stdout = subprocess.check_output(['sudo', 'service', 'gpspipe-nmea', 'start']).decode('utf-8')
        stdout += subprocess.check_output(['sudo', 'service', 'gpspipe-raw', 'start']).decode('utf-8')
    elif "stop" in request.form:
        stdout = subprocess.check_output(['sudo', 'service', 'gpspipe-nmea', 'stop']).decode('utf-8')
        stdout += subprocess.check_output(['sudo', 'service', 'gpspipe-raw', 'stop']).decode('utf-8')
    elif "shutdown" in request.form:
        stdout = subprocess.check_output(['sudo', 'shutdown', '-h', '+1']).decode('utf-8')
    elif "shutdown_cancel" in request.form:
        stdout = subprocess.check_output(['sudo', 'shutdown', '-c']).decode('utf-8')
    return render_template('button.html.j2',
                           terminal_output=stdout)


@app.route('/api/board_params/light', methods=['GET'])
def query_board_params_light():
    data = get_board_info()
    return data


@app.route('/api/board_params/full', methods=['GET'])
def query_board_params_full():
    data = get_board_info()
    return data


@app.route('/api/files/list', methods=['GET'])
def query_files_list():
    _, files_list = get_files_sort_max('/tmp/gpspipe')
    return files_list


@app.route('/api/files/mtime_diff', methods=['GET'])
def query_files_mtime_diff():
    diff_max_mtime, _ = get_files_sort_max('/tmp/gpspipe')
    return diff_max_mtime


@app.route('/api/shutdown', methods=['GET'])
def api_shutdown():
    try:
        output = subprocess.check_output(['sudo', 'shutdown', '-h', '+1']).decode('utf-8')
    except subprocess.CalledProcessError as e:
        output = e.output.decode()
    return output
