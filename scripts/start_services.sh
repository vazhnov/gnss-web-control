#!/usr/bin/env bash
set -o nounset
set -o errexit
set -o pipefail
shopt -s dotglob

# This script should be added to /etc/rc.local with "&" symbol at the end, to not wait when it finishes.
# These services have to be disabled:
# sudo systemctl disable gpspipe-nmea.service
# sudo systemctl disable gpspipe-raw.service

NET_DEV='eth0'
# shellcheck disable=SC1091   # Not following: /etc/default/gnss_scripts was not specified as input (see shellcheck -x).
source /etc/default/gnss_scripts

# Not mandatory in this script, but just for a better comfort when run something manually:
mkdir -p "$NMEA_LOG_DIR"
chown gnss_scripts:gnss_scripts "$NMEA_LOG_DIR"

# Check Ethernet RJ45 cable is connected:

# Wait till `/carrier` is ready, or `cat` returns an error "Invalid argument".
# TODO: it is better to do with a hook in /etc/NetworkManager/dispatcher.d/
is_success='no'
declare -i counter=0
while [ "$is_success" == 'no' ]; do
  if [ ! "$(cat "/sys/class/net/$NET_DEV/carrier" 2>/dev/null)" ]; then
    echo "Can't read /sys/class/net/$NET_DEV/carrier, let's wait a little"
    sleep 3
  else
    echo "Can read /sys/class/net/$NET_DEV/carrier"
    is_success='yes'
  fi
  counter=$counter+1
  if [ "$counter" -gt 20 ]; then
    echo "Waited too long for /sys/class/net/$NET_DEV/carrier"
    exit 1
  fi
done

# Even if `/carrier` is readable, device is still not ready, it needs ~3 seconds till event
# NetworkManager: <info> device (eth0): carrier: link connected
declare -i counter=0
while [ $counter -lt 10 ]; do
  if [ "$(cat "/sys/class/net/$NET_DEV/carrier")" == '1' ]; then
    echo "Ethernet cable is connected, exit"
    # No need to record tracks if Ethernet cable is connected when OS just started - home mode
    service gpspipe-nmea stop
    service gpspipe-raw stop
    exit 0
  else
    counter=$counter+1
    sleep 1
  fi
done

# Wait for "3D fix"
echo "Ethernet cable is not connected, wait for 3D fix"
declare -i counter=0
while [ $counter -lt 120 ]; do
  # Possible values of 'State': NO FIX / 3D FIX
  if grep -q '3D FIX' '/tmp/webgps/gpsd-c.html'; then
    echo "3D fix established, it is time to start gpspipe"
    break
  fi
  counter=$counter+1
  sleep 1
done

echo "Starting gpspipe after $counter seconds of wait for 3D fix"
service gpspipe-nmea start
service gpspipe-raw start

# To reset satellits statistic, to have nice graph, because first seconds are usually wrong:
service webgps stop
rm -v /tmp/webgps/*
service webgps start
