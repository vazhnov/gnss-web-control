#!/usr/bin/env bash
set -o nounset
set -o errexit
set -o pipefail
shopt -s dotglob

# SPDX-License-Identifier: MIT

# This script should be executed every 5 minutes by a cron job `/etc/cron.d/check_battery_shutdown`.
# It checks if battery is discharging and battery level is more than 10%. If less, then start a system shutdown.
# Script uses `batteryinfo` function from `30-armbian-sysinfo` file of Armbian distribution.

# `/etc/cron.d/check_battery_shutdown` example:
# */5 * * * * root bash /var/lib/gnss_web_control/code-repository/scripts/cron_check_battery_shutdown.sh

BATTERY_PERCENT_MIN='10'
# `BATTERY_PERCENT_MIN` can be rewrited here:
source /etc/default/gnss_scripts

# Import a local copy of `/etc/update-motd.d/30-armbian-sysinfo`, with a change to only source functions code:
source "$(dirname -- "$0")/30-armbian-sysinfo"
batteryinfo

# `status_battery_text` has a leading whitespace
if [ "$status_battery_connected" == '1' ] && [[ "$status_battery_text" =~ [[:space:]]*discharging ]]; then
  if [ "$battery_percent" -lt "$BATTERY_PERCENT_MIN" ]; then
    logger --tag cron_check_battery_shutdown "battery_percent = $battery_percent, running shutdown"
    shutdown -h +1
  fi
fi
