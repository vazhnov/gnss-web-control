#!/usr/bin/env bash
set -o nounset
set -o errexit
set -o pipefail
shopt -s dotglob

usage()
{
cat << EOF
After the OS boot, time often is not synced. When first NMEA file is creating, it can receive wrong filename. The order of NMEA files can be wrong because of this.
This script renames files accordingly to information inside first GPZDA record in every file.

This script:
* reads every file PREFIX*.nmea,
* takes first timestamp in GPZDA field,
* checks if filename == PREFIX_YYYY-MM-DDTHH:MM:SS+00:00.nmea
* renames file

Usage: $0 <OPTIONS>
OPTIONS:
  PREFIX       prefix for NMEA files to read
  -h, --help   show this message and exit

See: https://wiki.openstreetmap.org/wiki/User:Vazhnov_Alexey/Record_GNSS_traces
EOF
}

if [ $# -ne 1 ]; then
  echo '[ERROR]: you have to set 1 argument: PREFIX' 1>&2
  usage
  exit 1
fi

PREFIX="$1"

for IN_FILE in ./"$PREFIX"*.nmea; do
  # shellcheck disable=SC2016   # Expressions don't expand in single quotes, use double quotes for that.
  FIRST_GPZDA="$(grep -m 1 '^\$GPZDA' "$IN_FILE")"
  # shellcheck disable=SC2034   # X appears unused. Verify use (or export if used externally).
  # Line example:
  # $GPZDA,084601.00,26,06,2021,00,00*6E
  IFS=',' read -r X G_TIME G_DAY G_MONTH G_YEAR X X <<< "$FIRST_GPZDA"
  G_HOUR="${G_TIME:0:2}"
  G_MIN="${G_TIME:2:2}"
  G_SEC="${G_TIME:4:2}"
  # echo "TIME: $G_TIME, DAY: $G_DAY, MONTH: $G_MONTH, YEAR: $G_YEAR"
  DST_NAME="./gpspipe_${G_YEAR}-${G_MONTH}-${G_DAY}T${G_HOUR}:${G_MIN}:${G_SEC}+00:00.nmea"
  if [ "$IN_FILE" == "$DST_NAME" ]; then
    echo "$DST_NAME — all OK, timestamp is the same"
  else
    echo "$DST_NAME — first timestamp is different than in filename, to be renamed to $DST_NAME"
    mv -v "$IN_FILE" "$DST_NAME"
  fi
done
