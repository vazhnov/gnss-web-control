# Web GNSS control panel

Stack: Python, Flask, shell.

See https://wiki.openstreetmap.org/wiki/User:Vazhnov_Alexey/Record_GNSS_traces/Bluetooth_and_web_panel

Screenshot:

![Screenshot main window](docs/Index_page_v0.4.png)

## Ansible installer

See [GNSS web control panel Ansible](https://gitlab.com/vazhnov/gnss-web-control-panel-ansible).

## How to run emulator

How to run:

```shell
mkdir -pv /tmp/1 && cd /tmp/1
git clone 'git@gitlab.com:vazhnov/gnss-web-control.git' 'code-repository'
cd code-repository
python3 -m venv venv-flask
source venv-flask/bin/activate
echo $VIRTUAL_ENV   # Check is it a virtual environment, should return a path
pip3 install -r requirements_emulator.txt
bash ./run_server_emulator.sh
curl 'http://[::1]:8080/'
```

How to stop:

```shell
CTRL+C
deactivate
echo $VIRTUAL_ENV   # Check is it not a virtual environment, should return nothing
```

## TODO: API

For [React JS](https://en.wikipedia.org/wiki/React_(JavaScript_library)) UI: https://github.com/annpryshchepa/track_app

* `/api/proc/stop?name=gpspipe`
* `/api/proc/start?name=gpspipe`
* `/api/shutdown`
* `/api/shutdown_cancel`
* `/api/board_params/full` — returns a dictionary: `{ power_ac: 0, power_battery: 2.37W }`
* `/api/board_params/light` — returns a dictionary: `{ power_battery: 2.37W }`
* `/api/files/list` — returns a list of dictionaries: `[ { 'name': 'abc', 'size': 123 }, { 'name': 'def', 'size': 456 } ]`
* `/api/files/mtime_diff` — returns one digit, diff in seconds: `0` or if no files: `null`

## Web-design

Thanks to [Hanna Pryshchepa](https://github.com/annpryshchepa) for a new fancy web-design with tabs!

## Copyright

MIT License for all files, except:

* [BSD-2-clause license](https://gitlab.com/gpsd/gpsd/-/blob/master/COPYING) for `gpsd-client/webgps.py`, GPSD project: [gpsd/contrib/webgps.py.in](https://gitlab.com/gpsd/gpsd/-/blob/master/contrib/webgps.py.in).
* GNU General Public License version 2 for `scripts/30-armbian-sysinfo`, Armbian Linux build tools: [packages/bsp/common/etc/update-motd.d/30-armbian-sysinfo](https://github.com/armbian/build/blob/master/packages/bsp/common/etc/update-motd.d/30-armbian-sysinfo).
