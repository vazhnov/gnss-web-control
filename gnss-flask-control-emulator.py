import datetime
from flask import Flask, render_template, request

# This Flask app emulates a work of `gnss-flask-control.py`, when you don't have a real hardware (Olimex Lime2).


def get_board_info():
    """
    Read raw data, normalize it (divide temperature to 1000, divide voltage/amperage to a million).
    Calculate power consumption for AC and battery.
    Based on `30-armbian-sysinfo` shell script.
    Should work with PMIC AXP209 and AXP803.
    """
    data = {
        'AC connected': 0,
        'Battery connected': 1,
        'Battery charging': 0,
        'Battery percent': 25,
        'AC voltage': 0,
        'AC amperage': 0,
        'Battery voltage': 3570000,
        'Battery amperage': 840000,
        'CPU temperature': 42900,
        'PMIC temperature': 47300
    }
    for name in data:
        if (name.endswith('voltage')) or (name.endswith('amperage')):
            data[name] = data[name] / 10**6
        elif name.endswith('temperature'):
            data[name] = data[name] / 10**3
    # Calculate power
    data['Power AC'] = data['AC voltage'] * data['AC amperage']
    data['Power battery'] = data['Battery voltage'] * data['Battery amperage']
    return data


def get_files_sort_max():
    files_list = [
        '2021-09-07 21:32:45 96132 gpspipe_2021-09-07T21:27:57+00:00.nmea',
        '2021-09-07 21:27:57 580787 gpspipe_2021-09-07T20:58:42+00:00.nmea',
        '2021-09-07 20:58:42 580189 gpspipe_2021-09-07T20:29:13+00:00.nmea',
        '2021-09-07 20:29:13 578134 gpspipe_2021-09-07T19:59:34+00:00.nmea',
        '2021-09-07 19:59:34 577372 gpspipe_2021-09-07T19:30:10+00:00.nmea',
        '2021-09-07 19:30:10 567272 gpspipe_2021-09-07T18:37:16+00:00.nmea'
    ]
    diff_max_mtime = 10
    return diff_max_mtime, files_list


def check_process(check_list: list):
    out = []
    for process_name in check_list:
        out.append('{} is <span style="background-color: green; color: grey;">running</span>'.format(process_name))
        # out.append('{} is <span style="background-color: red; color: grey;">not running</span>'.format(process_name))
    return out


app = Flask(__name__)


@app.route("/")
def index_page():
    diff_max_mtime, files_list = get_files_sort_max()
    time_now = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    return render_template('index.html.j2',
                           board_info=get_board_info(),
                           print_all_processes=check_process(['gpsd', 'gpspipe', 'hostapd']),
                           time_now=time_now,
                           diff_max_mtime=diff_max_mtime,
                           nmea_files=files_list)


@app.route("/favicon.ico")
def favicon_empty():
    return ''


@app.route('/button', methods=['GET', 'POST'])
def button():
    stdout = ''
    if "start" in request.form:
        stdout = ''
    elif "stop" in request.form:
        stdout = ''
    elif "shutdown" in request.form:
        stdout = ''
    elif "shutdown_cancel" in request.form:
        stdout = ''
    return render_template('button.html.j2',
                           terminal_output=stdout)
